package peterkrause.ckl_challenge10.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ArticleToRealm extends RealmObject{
    //@PrimaryKey
    //private int key;

    private String title;
    private String website;
    private String authors;
    private String date;
    private String content;
    private int tagId;
    private String tagLabel;
    private String image_url;

    public ArticleToRealm(){

    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getWebsite() {
        return website;
    }
    public void setWebsite(String website) {
        this.website = website;
    }

    public String getAuthors() {
        return authors;
    }
    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }

    public int getTagId() {
        return tagId;
    }
    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    public String getTagLabel() {
        return tagLabel;
    }
    public void setTagLabel(String tagLabel) {
        this.tagLabel = tagLabel;
    }

    public String getImage_url() {
        return image_url;
    }
    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
    /**
    public int getKey() {
        return key;
    }
    public void setKey(int key) {
        this.key = key;
    }
    **/
    public ArticleToRealm(String title, String website, String authors, String date, String content, int tagId, String tagLabel, String image_url) {
        this.title = title;
        this.website = website;
        this.authors = authors;
        this.date = date;
        this.content = content;
        this.tagId = tagId;
        this.tagLabel = tagLabel;
        this.image_url = image_url;
    }

}
