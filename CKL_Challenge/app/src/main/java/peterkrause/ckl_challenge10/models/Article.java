package peterkrause.ckl_challenge10.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

import io.realm.RealmObject;


public class Article implements Parcelable{
    @Expose
    private String title;
    @Expose
    private String website;
    @Expose
    private String authors;
    @Expose
    private String date;
    @Expose
    private String content;
    @Expose
    private ArticleTag[] tags;
    @Expose
    private String image_url;


    public Article(){

    }

    public Article(String title, String website, String authors, String date, String content, ArticleTag[] tags, String image_url) {
        this.title = title;
        this.website = website;
        this.authors = authors;
        this.date = date;
        this.content = content;
        this.tags = tags;
        this.image_url = image_url;
    }

    protected Article(Parcel in) {
        title = in.readString();
        website = in.readString();
        authors = in.readString();
        date = in.readString();
        content = in.readString();
        image_url = in.readString();
    }

    public static final Creator<Article> CREATOR = new Creator<Article>() {
        @Override
        public Article createFromParcel(Parcel in) {
            return new Article(in);
        }

        @Override
        public Article[] newArray(int size) {
            return new Article[size];
        }
    };

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public ArticleTag[] getTags() {
        return tags;
    }

    public ArticleTag getTag(){
        return tags[0];
    }

    public void setTags(ArticleTag[] tags) {
        this.tags = tags;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(website);
        dest.writeString(authors);
        dest.writeString(date);
        dest.writeString(content);
        dest.writeString(image_url);
    }


}
