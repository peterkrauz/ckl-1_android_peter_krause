package peterkrause.ckl_challenge10.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

import io.realm.RealmObject;

public class ArticleTag implements Parcelable{
    @Expose
    private int id;
    @Expose
    private String label;

    public ArticleTag(int id, String label) {
        this.id = id;
        this.label = label;
    }

    public ArticleTag(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    protected ArticleTag(Parcel in) {
        id = in.readInt();
        label = in.readString();
    }

    public static final Creator<ArticleTag> CREATOR = new Creator<ArticleTag>() {
        @Override
        public ArticleTag createFromParcel(Parcel in) {
            return new ArticleTag(in);
        }

        @Override
        public ArticleTag[] newArray(int size) {
            return new ArticleTag[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(label);
    }
}
