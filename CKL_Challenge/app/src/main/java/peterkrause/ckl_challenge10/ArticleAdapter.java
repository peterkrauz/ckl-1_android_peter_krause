package peterkrause.ckl_challenge10;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import peterkrause.ckl_challenge10.models.Article;
import peterkrause.ckl_challenge10.models.ArticleTag;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.Holder>{

    List<Article> articles;
    private ArticleClickListener listener;

    public void setOnArticleClickListener(ArticleClickListener articleClickListener) {
        listener = articleClickListener;
    }

    public ArticleAdapter(){
        articles = new ArrayList<>();
    }

    public void setArticles(List<Article> articles){
        this.articles = articles;
    }
    
    public List<Article> getArticles() {
        return articles;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View article = LayoutInflater.from(parent.getContext()).inflate(R.layout.article_cell,parent,false);
        return new Holder(article, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        Article currentArticle = articles.get(position);
        holder.title.setText(currentArticle.getTitle());
        holder.author.setText(currentArticle.getAuthors());
        holder.date.setText(currentArticle.getDate());
        Picasso.get().load(currentArticle.getImage_url()).resize(150,120).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    public void addArticle(Article article) {
        articles.add(article);
        notifyDataSetChanged();
    }

    public Article getSelectedArticle(int position) {
        return articles.get(position);
    }

    public ArticleTag getSelectedTag(int position){
        return getSelectedArticle(position).getTag();
    }

    public class Holder extends RecyclerView.ViewHolder{

        TextView title, author, date;
        CheckBox checkBox;
        ImageView image;

        public Holder(View itemView, final ArticleClickListener listener) {
            super(itemView);
            image = itemView.findViewById(R.id.articlePhotoId);
            title = itemView.findViewById(R.id.articleTitleId);
            author = itemView.findViewById(R.id.articleAuthorId);
            date = itemView.findViewById(R.id.articleDateId);
            checkBox = itemView.findViewById(R.id.checkBox);

            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkBox.setChecked(checkBox.isChecked());
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onClick(position);
                        }
                    }
                    checkBox.setChecked(true);
                }
            });
        }
    }

    public interface ArticleClickListener{
        void onClick(int position);
    }

}
