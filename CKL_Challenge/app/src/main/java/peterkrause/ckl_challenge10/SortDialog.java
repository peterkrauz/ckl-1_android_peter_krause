package peterkrause.ckl_challenge10;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import peterkrause.ckl_challenge10.ui.MainActivity;

public class SortDialog extends AppCompatDialogFragment {
    private RadioGroup radioGroup;
    private RadioButton radioButton;
    private SortDialogListener listener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.dialog_layout, null);
        radioGroup = view.findViewById(R.id.sorterRadioGroup);

        builder.setView(view)
                .setTitle("Sort Articles")
                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int sortParam = radioGroup.getCheckedRadioButtonId();
                        if (sortParam > 0){
                            radioButton = view.findViewById(sortParam);
                            String sortingPref = radioButton.getText().toString();
                            listener.applyPrefs(sortingPref);
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
            listener = (SortDialogListener) context;
    }

    public interface SortDialogListener{
        void applyPrefs(String preferences);
    }


}
