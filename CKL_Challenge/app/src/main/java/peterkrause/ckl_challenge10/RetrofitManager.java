package peterkrause.ckl_challenge10;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import peterkrause.ckl_challenge10.helpers.Constant;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitManager{
    private ArticleService articleService;

    public ArticleService getRetrofit(){
        if (articleService==null){
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            articleService = retrofit.create(ArticleService.class);
        }
        return articleService;
    }
}
