package peterkrause.ckl_challenge10;

import java.util.List;

import peterkrause.ckl_challenge10.models.Article;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ArticleService {
    @GET("challenge/")
    Call<List<Article>> getAllArticles();
}
