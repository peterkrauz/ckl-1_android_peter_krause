package peterkrause.ckl_challenge10.ui;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmMigrationNeededException;
import peterkrause.ckl_challenge10.ArticleAdapter;
import peterkrause.ckl_challenge10.R;
import peterkrause.ckl_challenge10.RetrofitManager;
import peterkrause.ckl_challenge10.SortDialog;
import peterkrause.ckl_challenge10.helpers.Utils;
import peterkrause.ckl_challenge10.models.Article;
import peterkrause.ckl_challenge10.models.ArticleTag;
import peterkrause.ckl_challenge10.models.ArticleToRealm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements SortDialog.SortDialogListener{

    private RecyclerView recycler;
    private RetrofitManager retrofitManager;
    private ArticleAdapter adapter;
    private List<Article> articles;
    private Button sortButton;
    private Button paramButton;
    private String sortingPref;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        configViews();
        Realm.init(this);
        // Realm.deleteRealm(Realm.getDefaultConfiguration());
        realm = Realm.getInstance(Realm.getDefaultConfiguration());


        if(Utils.isNetworkAvailable(getApplicationContext())){
            fetchArticles();
        } else {
            fetchArticlesFromRealm();
        }


    }

    private void openSortDialog(){
        SortDialog sortDialog = new SortDialog();
        sortDialog.show(getSupportFragmentManager(),"SortDialog");
    }

    private Article setArticleFromRealm(ArticleToRealm articleToRealm) {
        Article article = new Article();
        article.setTitle(articleToRealm.getTitle());
        article.setAuthors(articleToRealm.getAuthors());
        article.setContent(articleToRealm.getContent());
        article.setDate(articleToRealm.getDate());
        article.setImage_url(articleToRealm.getImage_url());
        article.setWebsite(articleToRealm.getWebsite());
        ArticleTag[] articleTags = new ArticleTag[]{new ArticleTag(articleToRealm.getTagId(),articleToRealm.getTagLabel())};
        article.setTags(articleTags);
        return article;
    }

    private void fetchArticlesFromRealm() {
        if(realm.isEmpty()){
            Toast.makeText(this, "You must first fetch articles online to fill the offline database!", Toast.LENGTH_SHORT).show();
        } else {
            realm.beginTransaction();
            RealmResults<ArticleToRealm> articleResults = realm.where(ArticleToRealm.class).findAll();
            while (articleResults.size() > 6) {
                articleResults.deleteLastFromRealm();
            }
            for (ArticleToRealm atr : articleResults) {
                adapter.addArticle(setArticleFromRealm(atr));
            }
            realm.commitTransaction();
            articles = adapter.getArticles();
        }
    }

    private void fetchArticles(){
        Call<List<Article>> listCall = retrofitManager.getRetrofit().getAllArticles();
        listCall.enqueue(new Callback<List<Article>>() {
            @Override
            public void onResponse(Call<List<Article>> call, Response<List<Article>> response) {
                if (response.isSuccessful()) {
                    articles = response.body();
                    for (int i = 0; i < articles.size(); i++) {
                        Article article = articles.get(i);
                        adapter.addArticle(article);

                        realm.beginTransaction();
                        ArticleToRealm realmArticle = realm.createObject(ArticleToRealm.class);
                        setArticleToRealm(realmArticle, article);
                        realm.commitTransaction();
                    }

                }
            }
            @Override
            public void onFailure(Call<List<Article>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Failure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setArticleToRealm(ArticleToRealm realmArticle, Article article) {
        realmArticle.setTitle(article.getTitle());
        realmArticle.setAuthors(article.getAuthors());
        realmArticle.setWebsite(article.getWebsite());
        realmArticle.setDate(article.getDate());
        realmArticle.setContent(article.getContent());
        realmArticle.setImage_url(article.getImage_url());
        realmArticle.setTagId(article.getTag().getId());
        realmArticle.setTagLabel(article.getTag().getLabel());
    }

    private void configViews() {
        retrofitManager = new RetrofitManager();
        sortButton = findViewById(R.id.sortButton);
        paramButton = findViewById(R.id.paramButton);
        recycler = findViewById(R.id.recyclerViewId);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        adapter = new ArticleAdapter();
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(layoutManager);
        recycler.setAdapter(adapter);
        adapter.setOnArticleClickListener(new ArticleAdapter.ArticleClickListener() {
            @Override
            public void onClick(int position) {
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra("Article", adapter.getSelectedArticle(position));
                intent.putExtra("Tags", adapter.getSelectedTag(position));
                startActivity(intent);
            }
        });
        sortButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sortingPref == null){
                    Toast.makeText(MainActivity.this, "Choose a sorting parameter first!", Toast.LENGTH_SHORT).show();
                } else {
                    sortArrayList(sortingPref);
                    Toast.makeText(MainActivity.this, "Articles sorted by "+sortingPref, Toast.LENGTH_SHORT).show();
                }
            }
        });
        paramButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSortDialog();
            }
        });

    }

    @Override
    public void applyPrefs(String preferences) {
        sortingPref = preferences;
        sortArticleList(sortingPref);
        Toast.makeText(this, "Articles sorted by "+sortingPref, Toast.LENGTH_SHORT).show();
    }

    public void sortArrayList(final String param){
            Collections.sort(articles, new Comparator<Article>() {
                @Override
                public int compare(Article o1, Article o2) {
                    int paramId = 0;
                    switch (param){
                        case "Title":
                            paramId = o1.getTitle().compareTo(o2.getTitle());
                            break;
                        case "Website":
                            paramId = o1.getWebsite().compareTo(o2.getWebsite());
                            break;
                        case "Author":
                            paramId = o1.getAuthors().compareTo(o2.getAuthors());
                            break;
                        case "Date":
                            paramId = o1.getDate().compareTo(o2.getDate());
                            break;
                        case "Content":
                            paramId = o1.getContent().compareTo(o2.getContent());
                            break;
                        case "Id":
                            paramId = Integer.toString(o1.getTag().getId()).compareTo(Integer.toString(o2.getTag().getId()));
                            break;
                        case "Label":
                            paramId = o1.getTag().getLabel().compareTo(o2.getTag().getLabel());
                            break;
                    }
                    return paramId;
                }
            });
        adapter.setArticles(articles);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adapter.setArticles(null);
        realm.close();
    }
}
