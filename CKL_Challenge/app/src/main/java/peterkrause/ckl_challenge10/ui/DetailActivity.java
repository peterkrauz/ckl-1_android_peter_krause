package peterkrause.ckl_challenge10.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import io.realm.Realm;
import peterkrause.ckl_challenge10.R;
import peterkrause.ckl_challenge10.models.Article;
import peterkrause.ckl_challenge10.models.ArticleTag;

public class DetailActivity extends AppCompatActivity {

    private ImageView articleImage;
    private TextView title, date, website, author, content, id, label;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        configViews();
        Intent intent = getIntent();
        Article article = intent.getParcelableExtra("Article");
        ArticleTag tag = intent.getParcelableExtra("Tags");
        setViews(article, tag); // setting the views with their respective article field
    }

    private void setViews(Article article, ArticleTag tag) {
        Picasso.get().load(article.getImage_url()).resize(250, 200).into(articleImage);
        title.setText(article.getTitle());
        date.setText(article.getDate());
        website.setText(article.getWebsite()+", ");
        author.setText(article.getAuthors());
        content.setText(article.getContent());
        id.setText(tag.getId()+", ");
        label.setText(tag.getLabel());
    }

    private void configViews() {
        articleImage = findViewById(R.id.articlePhoto);
        title = findViewById(R.id.articleTitle);
        date = findViewById(R.id.articleDate);
        website = findViewById(R.id.articleWebsite);
        author = findViewById(R.id.articleAuthor);
        content = findViewById(R.id.articleContent);
        id = findViewById(R.id.tagId);
        label = findViewById(R.id.tagLabel);
    }

}